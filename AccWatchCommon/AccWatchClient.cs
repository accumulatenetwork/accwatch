using System;
using System.Data;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Proto.API;
using Proto.Authentication;
using Grpc.Core;
using Grpc.Net.Client;

namespace AccWatchCommon
{
    public class AccWatchClient : IDisposable
    {
        private GrpcChannel Channel;
        private String HostURL;
        private GrpcChannelOptions Options;
        public AccWatchAPI.AccWatchAPIClient API { get; private set; }
        public Metadata Headers { get; private set; }
        
        public AccWatchClient(string host = "https://localhost:5001", GrpcChannelOptions options = null)
        {
            if (options == null)
            {
                options = new GrpcChannelOptions();
                
                if (host.Contains("localhost") || host.Contains("127.0.0."))  //Accept self
                {
                    options.HttpHandler = new HttpClientHandler()
                    {
                        ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                    };
                }
            }

            HostURL = host;
            Options = options;
        }

        public Metadata Connect()
        {
            Channel = GrpcChannel.ForAddress(HostURL,Options);
            API =  new AccWatchAPI.AccWatchAPIClient(Channel);
            var authenticationClient = new Proto.Authentication.AccWatchAuthentication.AccWatchAuthenticationClient(Channel);
            var authenticationReply = authenticationClient.Authenticate(new AuthenticationRequest()
            {
                Username = "admin",
                Password = "admin"
            });

            Console.WriteLine($"Token: {authenticationReply.AccessToken}");
            
            Headers = new Metadata();
            Headers.Add("Authorization",$"Bearer {authenticationReply.AccessToken}");

            return Headers;
        }

        public void Dispose()
        {
            
            Channel.ShutdownAsync().Wait();
            Channel.Dispose();
        }
        
    }
}