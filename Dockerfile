FROM mcr.microsoft.com/dotnet/sdk:7.0-jammy-amd64 AS builder

COPY . /AccWatch
WORKDIR /AccWatch

RUN GITHASH=$(git rev-parse HEAD)
RUN GITTAG=$(git describe --tags | sed 's/-g.*//')
RUN GITBRANCH=$(git rev-parse --abbrev-ref HEAD)
RUN GITDATE=$(git show -s --date='unix' --format=%cd HEAD)

RUN dotnet build AccWatch.sln --configuration Release -p:GitHash=$(git rev-parse HEAD) -p:GitBranch=$(git rev-parse --abbrev-ref HEAD) -p:GitDateTime=$(git show -s --date='unix' --format=%cd HEAD) -p:GitTag=$(git describe --tags | sed 's/-g.*//') --output /app

WORKDIR /
RUN apt update && apt install unzip
RUN wget https://gitlab.com/api/v4/projects/41009693/jobs/artifacts/main/download?job=build -O www.zip
RUN unzip www.zip

FROM mcr.microsoft.com/dotnet/aspnet:7.0-jammy-amd64 AS run

COPY --from=builder /app /app
COPY --from=builder /www /www

# Set the timezone.
ENV TZ=UTC
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime

RUN apt update \
	&& apt -y install joe less ssh wget curl mtr-tiny bash

RUN apt clean
RUN rm -rf /var/lib/apt/lists/*

ENV AccWatch_DATA = "/data"
ENV AccWatch_WWW = "/www"
ENV HOME = /data

EXPOSE 16666
WORKDIR /app
ENTRYPOINT ["/usr/bin/dotnet","AccWatch.dll"]
