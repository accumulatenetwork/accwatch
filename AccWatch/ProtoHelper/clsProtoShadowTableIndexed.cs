using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Grpc.Core;
using Proto.API;


namespace ProtoHelper;
/// <summary>
/// Dictionary of proto messages, that are twinned with another class (a shadow class).
/// If the TIndex value is set to default (i.e 0 for primitives), then one will be assigned.
/// </summary>
/// <typeparam name="TProtoS">Our shadow class</typeparam>
/// <typeparam name="TProto">The proto message class, we want to extend</typeparam>
public class clsProtoShadowTableIndexed<TProtoS,TProto,TIndex> : IDisposable where TProto:IMessage where TProtoS:IProtoShadowClass<TIndex,TProto>
{
    private Dictionary<TIndex,TProtoS> ShadowDictionary = new Dictionary<TIndex,TProtoS>();

    private TProtoS[] ShadowList = Array.Empty<TProtoS>();

    private RepeatedField<TProto> ProtoRepeatedField { get; init; }
    private Func<TProto, IComparable<TIndex>> IndexSelector;
    private Func<TProto, TIndex, TIndex> NewIndex;

    public Action SaveAction { get; set; }
    private TIndex MaxIndex;

    EventWaitHandle NewMessageWait = new EventWaitHandle(false, EventResetMode.ManualReset);
    public event EventHandler<TProto> NewMessage;
    
    private readonly object _lock = new object();

    private TProto? _lastMessage;
    private TProto? LastMessage
    {
        get
        {
            return _lastMessage;
        }
        set
        {
            _lastMessage = value;
            NewMessageWait.Set();
            NewMessageWait.Reset();
        }
    }


    public delegate void MapFields(TProto origMessage, TProto newMessage);
    
    public clsProtoShadowTableIndexed(Func<TProto, IComparable<TIndex>> indexSelector, Func<TProto, TIndex, TIndex> newIndex) : base()
    {
        ShadowDictionary = new Dictionary<TIndex, TProtoS>();
        ProtoRepeatedField = new RepeatedField<TProto>();
        IndexSelector = indexSelector;
        NewIndex = newIndex;
    }

    public RepeatedField<TProto> GetProtoRepeatedField()
    {
        lock (_lock)
        {
            return ProtoRepeatedField.Clone();
        }
    }

    public TProtoS[] GetShadowArray()
    {
        lock (_lock)
        {
            return ShadowList;
        }
    }


    public void PopulateRepeatedField (RepeatedField<TProto> rp)
    {
        lock (_lock)
        {
            rp.AddRange(ProtoRepeatedField);
        }
    }

    public bool Update(TProto protoMessage, Action<TProto, TProto>? mapfields = null)
    {
        bool success;
        var id = (TIndex)IndexSelector(protoMessage);
        lock (_lock)
        {
            success = Update(id,protoMessage, mapfields);
        }

        if (success)
        {
            NewMessage?.Invoke(this, protoMessage);
            LastMessage = protoMessage;
        }
        return success;
    }

    public bool Exists(TProto protoMessage)
    {
        lock (_lock)
        {
            return ShadowDictionary.ContainsKey((TIndex)IndexSelector(protoMessage));
        }
    }

    public void Update(RepeatedField<TProto> repeatedField,Action<TProto,TProto>? mapfields = null)
    {
        lock (_lock)
        {
            foreach (var protoMessage in repeatedField)
            {
                var id = (TIndex)IndexSelector(protoMessage);

                if(EqualityComparer<TIndex>.Default.Equals(id, default(TIndex))) //ID not set
                {
                    if (ShadowDictionary.ContainsKey(id))
                        Update(id, protoMessage, mapfields);
               //     else
              //          AddPrivate(id, protoMessage);
                }
            }
        }
    }
    
    private bool Update(TIndex id, TProto newMessage,Action<TProto,TProto>? mapfields = null)
    {
        if (!ShadowDictionary.ContainsKey(id)) return false;

       TProto origMessage = ShadowDictionary[id].ProtoMessage;

        if (mapfields == null)
        {
            var originalProperties = origMessage.GetType().GetProperties();

            //For each updated property
            foreach (var updateProperty in newMessage.GetType().GetProperties())
            {
                var originalProperty = originalProperties.FirstOrDefault(x =>
                    x.Name == updateProperty.Name && x.GetValue(x) == updateProperty.GetValue(updateProperty));
                if (originalProperty != null)
                    originalProperty.SetValue(originalProperty, updateProperty.GetValue(updateProperty));
            }
        }
        else
        {
            mapfields(origMessage, newMessage);
        }
        SaveAction?.Invoke();
        return true;
    }
    
    /*/// <summary>
    /// Add bulk items.  Note: No Events triggered here, for subscribed consumers.  
    /// </summary>
    /// <param name="repeatedField"></param>
    public new void Add(RepeatedField<TProto> repeatedField)
    {
        lock (_lock)
        {
            foreach (var protoMessage in repeatedField)
            {
                var id = (UInt64)IndexSelector(protoMessage);
                if (id != 0) //ID not set
                {
                    var valueClass = (TProtoS)Activator.CreateInstance(typeof(TProtoS), protoMessage);
                    base.Add(id, valueClass);
                }
            }
            
            ProtoRepeatedField.AddRange(repeatedField);
        }
    }*/

    public TProtoS Add(TProto value)
    {
        var shadowClass = (TProtoS)Activator.CreateInstance(typeof(TProtoS), value);
        return Add(value, shadowClass);
    }

    public TProtoS Add(TProto value, TProtoS shadowClass)
    {
        var ID = (TIndex)IndexSelector(value);
        if (!shadowClass.ID.Equals(ID)) throw new Exception("ID Mismatch - check shadow class");

        lock (_lock)
        {
            if (EqualityComparer<TIndex>.Default.Equals(ID, default(TIndex)))
            {
                if (NewIndex == null) throw new Exception("Index not set - cannot create one");
                ID = MaxIndex = NewIndex.Invoke(value, MaxIndex); //New index is required
            }
            else
            {
                if (NewIndex != null && Comparer<TIndex>.Default.Compare(ID, MaxIndex) >= 0) MaxIndex = ID;
            }

            if (!ShadowDictionary.TryAdd(ID, shadowClass)) return default;
            ShadowList = ShadowDictionary.Values.ToArray();
            ProtoRepeatedField.Add(value);
            SaveAction?.Invoke();
        }
        LastMessage = value;
        NewMessage?.Invoke(this,value);
        return shadowClass;
    }

    public bool Remove(TProto value)
    {
        return Remove((TIndex)IndexSelector(value));
    }
    
    public bool Remove(TIndex id)
    {
        lock (_lock)
        {
            if (ShadowDictionary.Remove(id))
            {
                var value = ProtoRepeatedField.FirstOrDefault(x => IndexSelector(x).Equals(id));
                if (value != null) ProtoRepeatedField.Remove(value);
                NewMessage?.Invoke(this,value);
                LastMessage = value;
                ShadowList = ShadowDictionary.Values.ToArray();
                SaveAction?.Invoke();
                return true;
            }
            SaveAction?.Invoke();
            return false;
        }
    }

    public int Count
    {
        get
        {
            return ShadowDictionary.Count;
        }
    }

    public TProtoS? GetValueS(TIndex id)
    {
        if (TryGetValueS(id, out var shadowClass))
        {
            return shadowClass;
        }
        throw null;
    }

    public bool TryGetValueS(TIndex index, out TProtoS shadowClass)
    {
        lock (_lock)
        {
            return ShadowDictionary.TryGetValue(index, out shadowClass);
        }
    }

    public async Task NextMessageWait(IServerStreamWriter<TProto> serverStreamWriter)
    {
        while (NewMessageWait.WaitOne())
        {
            await serverStreamWriter.WriteAsync(LastMessage);
        }
    }

    /*public async void Load<TProtoList>(string path, Func<RepeatedField<TProto>> defaultData = null)
    {
        RepeatedField<TProto> networkListProto;
        var parser = new Google.Protobuf.MessageParser<TProtoList>(() => ProtoWrapper);
        if (File.Exists(path))
        {
            //Read from file
            networkListProto = parser.ParseFrom(File.ReadAllBytes(path));
            ManagerList.Add(RepeatedFieldSelector(networkListProto));
        }
        else if (defaultData != null)
        {
            networkListProto = defaultData();
            ManagerList.Add(RepeatedFieldSelector(networkListProto));
        }
    }

    private void Save<TProtoList>(string path,Func<TProtoList, IComparable<TIndex>> indexSelector)
    {
        TProtoList protoList = new TProtoList();

        File.WriteAllBytes(path,  GetProtoRepeatedField().ToByteArray());
    }
*/
    public void Dispose()
    {
        NewMessageWait.Dispose();
    }

}