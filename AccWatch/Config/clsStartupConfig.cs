using System.IdentityModel.Tokens.Jwt;
using Proto.Settings;
using Serilog;
using SharpYaml.Serialization;
using Serializer = SharpYaml.Serialization.Serializer;

namespace AccWatch;

public static class clsStartupConfig
{
    public static Proto.Settings.Settings Settings { get; private set;}

    public static string DataPath
    {
        get
        {
            return Path.Combine(Program.DataPath, "config.yaml");
        }
    }


    public static bool Load(string dataPath = null)
    {
        if (String.IsNullOrEmpty(dataPath)) dataPath = DataPath;

        if (!File.Exists(dataPath))
        {
#if DEBUG
            Settings = new Settings
            {
                WWWPort = 8080,
            };
            Settings.CorsURL.Add("https://*");
#else
            Settings = new Settings
            {
                WWWPort = 443,
            };
#endif
        }
        else
        {
            //Read from file
            try
            {
                Log.Information("Loading {path}",dataPath);
                using (var readFile = new StreamReader(dataPath))
                {
                    var serializer = new Serializer();
                    Settings = serializer.Deserialize<Proto.Settings.Settings>(readFile);
                }
            }
            catch (Exception e)
            {
                Log.Error(e,"Error");
                return false;
            }
        }

        return true;

    }

    public static void Save(string dataPath = null)
    {
        if (String.IsNullOrEmpty(dataPath)) dataPath = DataPath;

        using (var writefile = new StreamWriter(dataPath))
        {
            var serializer = new Serializer();
            serializer.Serialize(writefile, Settings, typeof(Proto.Settings.Settings));
        }
    }
}