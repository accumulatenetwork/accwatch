using System.Xml.Linq;
using AccWatch.Monitoring;
using Docker.DotNet.Models;
using Google.Protobuf;
using Proto.API;
using ProtoHelper;

namespace AccWatch;

public static class clsSettings
{
    static public string DataPath = Path.Combine(Program.DataPath, "settings.dat");
    
    
    public static void Load()
    {
        GetSettings();
    }
    
    
    private static void GetSettings()
    {
            
        if (File.Exists(Path.Combine(Program.DataPath,"settings")))
        {  //Read from file
            Program.APISettings = Settings.Parser.ParseFrom(File.ReadAllBytes(Path.Combine(Program.DataPath, "settings.dat")));
        }
        else
        {
            SetDefaults();
        }
    }


    static public void Update(Proto.API.Settings newSettigs)
    {
        bool flagRestartDiscord = false;
        
        var originalProperties = Program.APISettings.GetType().GetProperties();

        //For each updated property
        foreach (var updateProperty in newSettigs.GetType().GetProperties())
        {
            var originalProperty = originalProperties.FirstOrDefault(x =>
                x.Name == updateProperty.Name && x.GetValue(x) == updateProperty.GetValue(updateProperty));
            if (originalProperty != null)
            {
                switch (updateProperty.Name)
                {
                    case nameof(Program.APISettings.DiscordToken):
                    case nameof(Program.APISettings.DiscordClientID):
                        flagRestartDiscord = true;
                        break;
                }
                originalProperty.SetValue(originalProperty, updateProperty.GetValue(updateProperty));
            }
        }

      //  if (flagRestartDiscord) Program.Bot.RunAsync();

    }
    

    static public void Save()
    {
        // Write to file
        File.WriteAllBytes(DataPath,Program.APISettings.ToByteArray());
        
    }


    static void SetDefaults()
    {

        //Fill with defaults
        Program.APISettings = new Settings()
        {
            BotName = "My Bot",
            DiscordClientID = "955503128705392660",
            DiscordToken = "OTU1NTAzMTI4NzA1MzkyNjYw.Yjinog.ltCgJ9c_-SXlLEDPR7khzTSJBa0",
            AccumulateOperatorAlertsCh = 443025488655417364,
            DiscordAlertsChannel = "#Bot-Alerts",
            SIPUsername = "",
            SIPPassword = "",
            SIPHost = "",
            SIPCallingNumber = "",
            TwimletURL = "",
            AlarmOffWarningMinutes = 30,
            LatencyTriggerMultiplier = 2,
            BotCommandPrefix = "!",
            EmailSMTPHost = "",
            EmailSMTPPort = 587,
            EmailUsername = "",
            EmailPassword = "",
            EmailFromAddress = "",
        };


    }

    
}