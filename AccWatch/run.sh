docker build . -t AccWatch --no-cache
docker run -d \
       -v "AccWatch:/data" \
       -v "AccWatch-www:/www" \
       -v "/var/run/docker.sock:/var/run/docker.sock" \
       -e AccWatch_WWW=/www \
       -e AccWatch_DATA=/data \
       -e AccWatch_DOMAIN="red2.logicethos.com" \
       --network host --name AccWatch AccWatch
