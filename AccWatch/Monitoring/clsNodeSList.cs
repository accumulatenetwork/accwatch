using AccWatch;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Org.BouncyCastle.Crypto.Tls;
using Proto.API;
using ProtoHelper;

namespace AccWatch.Monitoring;

using TIndex = UInt32;
using TProto = Proto.API.Node;
using TProtoS = clsNode;
using TProtoList = Proto.API.NodeList;

//<TProtoList,TProto,TProtoS,TIndex>
//<Proto.API.NodeList, Proto.API.Node,clsNode,UInt32>

public class clsNodeSList : clsProtoShadowTableIndexed<TProtoS, TProto, TIndex>
{
    string FilePath = Path.Combine(Program.DataPath, "NodeList");
    public ProtoPubSub<Proto.API.NodeStatus> NodeStatusSubSub = new ProtoPubSub<NodeStatus>();

    private static Func<TProto,TIndex, TIndex> NewIndex = new Func<TProto,TIndex, TIndex>((x, y) => { return x.NodeID = ++y; });

    private Action<TProto, TProto> MapFields = null;

    public clsNodeSList() : base(new Func<TProto, IComparable<TIndex>>(x => x.NodeID)
                                            ,NewIndex)
    {
        
        MapFields = new Action<Proto.API.Node, Proto.API.Node>((origMessage, newMessage) =>
        {
            origMessage.Name = newMessage.Name;
            origMessage.Host = newMessage.Host;
            origMessage.Monitor = newMessage.Monitor;
            origMessage.NodeGroupID = newMessage.NodeGroupID;
        });

      //  var indexSelector = new Func<TProto, IComparable<TIndex>>(x => x.NodeID); //Index field of our proto message
      //  var indexSelectorWrite = new Action<TProto, TIndex>((x, y) => x.NodeID = y); //Write action for index field.

       // NodeShadowList = new clsProtoShadowTableIndexed<TProtoS, TProto, TIndex>(indexSelector,indexSelectorWrite);

       base.SaveAction = new Action(() =>
       {
           var list = new TProtoList();
           list.Nodes.AddRange(this.GetProtoRepeatedField());
           File.WriteAllBytes(FilePath, list.ToByteArray());
       });

       Load();
    }


    public TProtoS Add(TProto node)
    {
        if (node.NodeID != 0) throw new Exception($"{nameof(this.GetType)} Add, with index already set");
        return AddBase(node);
    }
    private TProtoS AddBase(TProto node)
    {
        return base.Add(node, new clsNode(node));
    }


    public bool Update(TProto nodeGroup)
    {
        return base.Update(nodeGroup,MapFields);
    }
    public MsgReply Remove(TIndex id)
    {
        var msgReply = new MsgReply();
        msgReply.Status = base.Remove(id) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        return msgReply;
    }

    public MsgReply AddUpdate(Node node)
    {
        var msgReply = new MsgReply();

        if (node.NodeID == 0)
        {
            var shadowClass = Add(node);
            if (shadowClass == null)
            {
                msgReply.Status = MsgReply.Types.Status.Fail;
            }
            else
            {
                msgReply.Status = MsgReply.Types.Status.Ok;
                msgReply.NewID32 = shadowClass.ID;
            }
        }
        else
        {
            msgReply.Status = Update(node) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }

        return msgReply;
    }


    public void Load()
    {
        if (File.Exists(FilePath))
        {
            var parser = new Google.Protobuf.MessageParser<TProtoList>(() => new NodeList());
            {
                var nodelist = parser.ParseFrom(File.ReadAllBytes(FilePath));
                foreach (var node in nodelist.Nodes)
                {
                    AddBase(node);
                }
            }
        }
        else
        {
            LoadDefault();
        }
    }

    public void ClearDataFile()
    {
        if (File.Exists(FilePath))
        {
            ALog.Info($"Deleting {FilePath}");
            File.Delete(FilePath);
        }
    }

    public void LoadDefault()
    {
        ALog.Info($"LoadDefault {FilePath}");
        AddBase(new Node()
        {
            NodeID = 1,
            NodeGroupID = 1,
            Name = "NY Node",
            Host = "100.23.123.12",
            Monitor = false,
        });
        AddBase(new Node()
        {
            NodeID = 2,
            NodeGroupID = 1,
            Name = "London Node",
            Host = "10.3.44.88",
            Monitor = false,
        });
        AddBase(new Node()
        {
            NodeID = 3,
            NodeGroupID = 2,
            Name = "Frankfurt Node",
            Host = "155.22.14.184",
            Monitor = false,
        });
    }
}