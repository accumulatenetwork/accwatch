using AccWatch;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Proto.API;
using ProtoHelper;

namespace AccWatch.Monitoring;

using TIndex = UInt32;
using TProto = Proto.API.NotificationPolicy;
using TProtoS = clsNotificationPolicy;
using TProtoList = Proto.API.NotificationPolicyList;

public class clsNotificationPolicySList : clsProtoShadowTableIndexed<TProtoS, TProto, TIndex>
{

    string FilePath = Path.Combine(Program.DataPath, "NotificationPolicyList");
    private Action<TProto, TProto> MapFields = null;

    private static Func<TProto,IComparable<TIndex>> IndexSelector = new Func<TProto, IComparable<TIndex>>(x => x.NotifictionID);
  //  private static Action<TProto, TIndex> IndexSelectorWrite = new Action<TProto, TIndex>((x, y) => x.NotifictionID = y);
    private static Func<TProto,TIndex, TIndex> NewIndex = new Func<TProto,TIndex, TIndex>((x, y) => { return x.NotifictionID = ++y; });

    public clsNotificationPolicySList(): base(IndexSelector, NewIndex)
    {
        base.SaveAction = new Action(() =>
        {
            var list = new TProtoList();
            list.NotificationPolicyList_.AddRange(this.GetProtoRepeatedField());
            File.WriteAllBytes(FilePath, list.ToByteArray());
        });

        Load();
    }

    // void SetNextID(TProto proto )
    // {
    //     lock (_idLock)
    //     {
    //         MaxID++;
    //         IndexSelectorWrite.Invoke(proto,MaxID);
    //     }
    // }

    public TProtoS Add(TProto notificationPolicy)
    {
        if (notificationPolicy.NotifictionID != 0) throw new Exception($"{nameof(this.GetType)} Add, with index already set");
        return AddBase(notificationPolicy);
    }
    private TProtoS AddBase(TProto notificationPolicy)
    {
        return base.Add(notificationPolicy, new clsNotificationPolicy(notificationPolicy));
    }

    public bool Update(TProto nodeGroup)
    {
        return base.Update(nodeGroup,MapFields);
    }


    public MsgReply Remove(TIndex id)
    {
        var msgReply = new MsgReply();

        if (Program.NodeGroupSList.GetShadowArray().Any(x => x.ProtoMessage.LatencyNotifictionID == id ||
                                            x.ProtoMessage.HeightNotifictionID == id ||
                                            x.ProtoMessage.PingNotifictionID == id))
        {
            msgReply.Status = MsgReply.Types.Status.Fail;
            msgReply.Message = "Cannot delete. Notification timer is in use by Node Group";
        }
        else
        {
            msgReply.Status = base.Remove(id) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }

        return msgReply;
    }


    public MsgReply AddUpdate(NotificationPolicy notificationPolicy)
    {
        var msgReply = new MsgReply();

        if (notificationPolicy.NotifictionID == 0)
        {
            var shadowClass = Add(notificationPolicy);
            if (shadowClass == null)
            {
                msgReply.Status = MsgReply.Types.Status.Fail;
            }
            else
            {
                msgReply.Status = MsgReply.Types.Status.Ok;
                msgReply.NewID32 = shadowClass.ID;
            }
        }
        else
        {
            msgReply.Status = Update(notificationPolicy) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }

        return msgReply;
    }

    public void Save()
    {
        var list = new TProtoList();
        list.NotificationPolicyList_.AddRange(this.GetProtoRepeatedField());
        File.WriteAllBytes(FilePath, list.ToByteArray());
    }

    public void Load()
    {
        if (File.Exists(FilePath))
        {
            var parser = new Google.Protobuf.MessageParser<TProtoList>(() => new NotificationPolicyList());
            {
                var notificationPolicyList = parser.ParseFrom(File.ReadAllBytes(FilePath));
                foreach (var notificationPolicy in notificationPolicyList.NotificationPolicyList_)
                {
                    AddBase(notificationPolicy);
                }
            }
        }
        else
        {
            LoadDefault();
        }
    }

    public void ClearDataFile()
    {
        if (File.Exists(FilePath))
        {
            ALog.Info($"Deleting {FilePath}");
            File.Delete(FilePath);
        }
    }

    public void LoadDefault()
    {

        ALog.Info($"LoadDefault {FilePath}");
        AddBase(new NotificationPolicy
        {
            NotifictionID = 1,
            Name = "Nothing",
            Discord = -1,
            Call = -1
        });
        AddBase(new NotificationPolicy
        {
            NotifictionID = 2,
            Name = "Warning",
            Discord = 120,
            Call = -1
        });
        AddBase(new NotificationPolicy
        {
            NotifictionID = 3,
            Name = "Alert",
            Discord = 0,
            Call = 600
        });
        AddBase(new NotificationPolicy
        {
            NotifictionID = 4,
            Name = "Panic",
            Discord = 0,
            Call = 0
        });
    }

}