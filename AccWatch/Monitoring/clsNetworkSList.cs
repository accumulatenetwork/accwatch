using AccWatch;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Newtonsoft.Json;
using Proto.API;
using ProtoHelper;

namespace AccWatch.Monitoring;

using TIndex = UInt32;
using TProto = Proto.API.Network;
using TProtoS = clsNetwork;
using TProtoList = Proto.API.NetworkList;

public class clsNetworkSList : clsProtoShadowTableIndexed<TProtoS, TProto, TIndex>
{
    string FilePath = Path.Combine(Program.DataPath, "NetworkList");
    public ProtoPubSub<Proto.API.NetworkStatus> NetworkStatusSubSub = new ProtoPubSub<NetworkStatus>();
    private Action<TProto, TProto> MapFields = null;

    private static Func<TProto,TIndex, TIndex> NewIndex = new Func<TProto,TIndex, TIndex>((x, y) => { return x.NetworkID = ++y; });

    public clsNetworkSList()  : base(
        new Func<TProto, IComparable<TIndex>>(x => x.NetworkID)
        ,NewIndex)
    {
        MapFields = new Action<Proto.API.Network, Proto.API.Network>((origMessage, newMessage) =>
        {
            origMessage.Name = newMessage.Name;
            origMessage.BlockTime = newMessage.BlockTime;
            origMessage.StalledAfter = newMessage.StalledAfter;
            origMessage.NotifictionID = newMessage.NotifictionID;
        });

        base.SaveAction = new Action(() =>
        {
            var list = new TProtoList();
            list.Network.AddRange(this.GetProtoRepeatedField());
            File.WriteAllBytes(FilePath, list.ToByteArray());
        });

        Load();
    }

    public TProtoS Add(TProto node)
    {
        if (node.NetworkID != 0) throw new Exception($"{nameof(this.GetType)} Add, with index already set");
        return AddBase(node);
    }
    private TProtoS AddBase(TProto node)
    {
        return base.Add(node, new clsNetwork(node));
    }

    public bool Update(TProto network)
    {
        return base.Update(network,MapFields);
    }


    public MsgReply Remove(TIndex id)
    {
        var msgReply = new MsgReply();
        if (Program.NodeGroupSList.GetShadowArray().Any(x => x.ProtoMessage.NetworkID == id))
        {
            msgReply.Status = MsgReply.Types.Status.Fail;
            msgReply.Message = "Cannot delete. Network in use.";
        }
        else
        {
            msgReply.Status = base.Remove(id) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }
        ALog.Info(JsonConvert.SerializeObject(msgReply));
        return msgReply;
    }

    public MsgReply AddUpdate(Network network)
    {
        var msgReply = new MsgReply();

        if (network.NetworkID == 0)
        {
            var shadowClass = Add(network);
            if (shadowClass == null)
            {
                msgReply.Status = MsgReply.Types.Status.Fail;
            }
            else
            {
                msgReply.Status = MsgReply.Types.Status.Ok;
                msgReply.NewID32 = shadowClass.ID;
            }
        }
        else
        {
            msgReply.Status = Update(network) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }

        return msgReply;
    }


    public void Load()
    {
        if (File.Exists(FilePath))
        {
            var parser = new Google.Protobuf.MessageParser<TProtoList>(() => new NetworkList());
            {
                var networkList = parser.ParseFrom(File.ReadAllBytes(FilePath));
                foreach (var node in networkList.Network)
                {
                    AddBase(node);
                }
            }
        }
        else
        {
            LoadDefault();
        }
    }

    public void ClearDataFile()
    {
        if (File.Exists(FilePath))
        {
            ALog.Info($"Deleting {FilePath}");
            File.Delete(FilePath);
        }
    }

    public void LoadDefault()
    {
          ALog.Info($"LoadDefault {FilePath}");
          ALog.Info(nameof(Type),$"Deleting {FilePath}");

          AddBase(new Network
            {
                NetworkID = 1,
                Name = "Mainnet",
                StalledAfter = 60,
                BlockTime = 600,
                NotifictionID = 0,
            });

          AddBase(new Network
            {
                NetworkID = 2,
                Name = "Testnet",
                StalledAfter = 300,
                BlockTime = 600,
                NotifictionID = 0,
            });
    }

}