using System.Collections.ObjectModel;
using AccWatch;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Proto.API;
using ProtoHelper;

namespace AccWatch.Monitoring;

using TIndex = UInt32;
using TProto = Proto.API.NodeGroup;
using TProtoS = clsNodeGroup;
using TProtoList = Proto.API.NodeGroupList;

public class clsNodeGroupSList : clsProtoShadowTableIndexed<TProtoS, TProto, TIndex>
{

    string FilePath = Path.Combine(Program.DataPath, "NodeGroupList");

    private Action<TProto, TProto> MapFields = null;

    private static Func<TProto,TIndex, TIndex> NewIndex = new Func<TProto,TIndex, TIndex>((x, y) => { return x.NodeGroupID = ++y; });

    public clsNodeGroupSList() : base(
        new Func<TProto, IComparable<TIndex>>(x => x.NodeGroupID)
        ,NewIndex)
    {

        MapFields = new Action<TProto, TProto>((origMessage, newMessage) =>
        {
            origMessage.Name = newMessage.Name;
            origMessage.NetworkID = newMessage.NetworkID;
            origMessage.HeightNotifictionID = newMessage.HeightNotifictionID;
            origMessage.LatencyNotifictionID = newMessage.LatencyNotifictionID;
            origMessage.PingNotifictionID = newMessage.PingNotifictionID;
        });

        base.SaveAction = new Action(() =>
        {
            var list = new TProtoList();
            list.NodeGroup.AddRange(this.GetProtoRepeatedField());
            File.WriteAllBytes(FilePath, list.ToByteArray());
        });


        Load();
    }


    public MsgReply AddUpdate(TProto nodeGroup)
    {
        var msgReply = new MsgReply();

        if (nodeGroup.NodeGroupID == 0)
        {
            var shadowClass = Add(nodeGroup);
            if (shadowClass == null)
            {
                msgReply.Status = MsgReply.Types.Status.Fail;
            }
            else
            {
                msgReply.Status = MsgReply.Types.Status.Ok;
                msgReply.NewID32 = shadowClass.ID;
            }
        }
        else
        {
            msgReply.Status = Update(nodeGroup) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }
        return msgReply;
    }

    public TProtoS Add(TProto nodeGroup)
    {
        if (nodeGroup.NodeGroupID != 0) throw new Exception($"{nameof(this.GetType)} Add, with index already set");
        return AddBase(nodeGroup);
    }
    public TProtoS AddBase(TProto nodeGroup)
    {
        return base.Add(nodeGroup, new clsNodeGroup(nodeGroup));
    }


    public bool Update(TProto nodeGroup)
    {
        return base.Update(nodeGroup,MapFields);
    }


    public MsgReply Remove(TIndex id)
    {
        var msgReply = new MsgReply();

        if (Program.NodeSList.GetShadowArray().Any(x => x.ProtoMessage.NodeGroupID == id))
        {
            msgReply.Status = MsgReply.Types.Status.Fail;
            msgReply.Message = "Cannot delete Network.  In use by nodes";
        }
        else
        {
            msgReply.Status = base.Remove(id) ? MsgReply.Types.Status.Ok : MsgReply.Types.Status.Fail;
        }
        return msgReply;
    }


    public void Load()
    {

        if (File.Exists(FilePath))
        {
            var parser = new Google.Protobuf.MessageParser<TProtoList>(() => new NodeGroupList());
            {
                var nodeGroupList = parser.ParseFrom(File.ReadAllBytes(FilePath));
                foreach (var node in nodeGroupList.NodeGroup)
                {
                    AddBase(node);
                }
            }
        }
        else
        {
            LoadDefault();
        }
    }

    public void ClearDataFile()
    {
        if (File.Exists(FilePath))
        {
            ALog.Info($"Deleting {FilePath}");
            File.Delete(FilePath);
        }
    }

    public void LoadDefault()
    {
        ALog.Info($"LoadDefault {FilePath}");
        AddBase(new NodeGroup()
        {
            NodeGroupID = 1,
            Name = "Mainnet Validator",
            NetworkID = 1,
            PingNotifictionID = 1,
            HeightNotifictionID = 1,
            LatencyNotifictionID = 1
        });

        AddBase(new NodeGroup
        {
            NodeGroupID = 2,
            Name = "Mainnet Follower",
            NetworkID = 1,
            PingNotifictionID = 1,
            HeightNotifictionID = 1,
            LatencyNotifictionID = 1
        });

        AddBase(new NodeGroup
        {
            NodeGroupID = 3,
            Name = "Testnet Validator",
            NetworkID = 1,
            PingNotifictionID = 1,
            HeightNotifictionID = 1,
            LatencyNotifictionID = 1
        });


        AddBase(new NodeGroup
        {
            NodeGroupID = 4,
            Name = "Testnet Follower",
            NetworkID = 1,
            PingNotifictionID = 1,
            HeightNotifictionID = 1,
            LatencyNotifictionID = 1
        });


    }


}