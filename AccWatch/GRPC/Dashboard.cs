using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Grpc.Core;
using Proto.API;
using Google.Protobuf.WellKnownTypes;
using System.IO;
using Google.Protobuf;
using Serilog;

namespace AccWatch.GRPC;

public partial class ApiService
{
    public override async Task NodeStatusStream(StreamRequest request, IServerStreamWriter<NodeStatus> responseStream, ServerCallContext context)
    {
        try
        {
            Console.WriteLine("NodeStatusStream started");
            while (!context.CancellationToken.IsCancellationRequested)
            {
                foreach (var node in Program.NodeSList.GetShadowArray())
                {
                    await responseStream.WriteAsync(node.NodeStatus);
                    Thread.Sleep(50); //<-- Flutter bug
                }
                await Task.Delay((int)request.Milliseconds, context.CancellationToken);
            }
            Console.WriteLine("NodeStatusStream End");
        }
        catch (TaskCanceledException)
        {
            Console.WriteLine("Connection Closed");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        
    }

    public override async Task NetworkStatusStream(StreamRequest request, IServerStreamWriter<NetworkStatus> responseStream, ServerCallContext context)
    {
        try
        {
            Console.WriteLine("NetworkStatusStream started");
            //context.CancellationToken.
            while (!context.CancellationToken.IsCancellationRequested)
            {
                foreach (var network in Program.NetworkSList.GetShadowArray())
                {
               //     Console.WriteLine("Send");
                    await responseStream.WriteAsync(network.Status);
                    Thread.Sleep(50); //<-- Flutter bug
                }
                await Task.Delay((int)request.Milliseconds, context.CancellationToken);

            }
            Console.WriteLine($"NetworkStatusStream End CanRequest: {context.CancellationToken.IsCancellationRequested}");
            ;
        }
        catch (TaskCanceledException)
        {
            Console.WriteLine("Connection Closed");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}