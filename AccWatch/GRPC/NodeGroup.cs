using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Grpc.Core;
using Proto.API;
using Google.Protobuf.WellKnownTypes;
using System.IO;
using Google.Protobuf;

namespace AccWatch.GRPC;

public partial class ApiService
{
     public override Task<MsgReply> NodeGroupSet(NodeGroup group, ServerCallContext context)
    {
        var msgReply = Program.NodeGroupSList.AddUpdate(group);

        return Task.FromResult(msgReply);
    }

    public override Task<NodeGroupList> NodeGroupListGet(Empty request, ServerCallContext context)
    {

        var proto = new NodeGroupList();
        Program.NodeGroupSList.PopulateRepeatedField(proto.NodeGroup);
        return Task.FromResult(proto);

    }

    public override Task<MsgReply> NodeGroupDelete(ID32 nodeGroupID, ServerCallContext context)
    {
        return Task.FromResult(Program.NodeGroupSList.Remove(nodeGroupID.ID));
    }

}