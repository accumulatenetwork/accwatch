using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccWatch.GRPC;
using LettuceEncrypt;
using LettuceEncrypt.Acme;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Events;
using Yarp.ReverseProxy.Configuration;

namespace AccWatch
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey =
                        new SymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtAuthenticationManager.JWT_TOKEN_KEY)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                };
            });
            services.AddAuthorization();
            services.AddGrpc();
            /*services.AddRazorPages()
                .AddRazorPagesOptions(options =>
                {
                    //Use the below line to change the default directory
                    //for your Razor Pages.
                    options.RootDirectory = Path.Combine("/",Program.DataPath, "www");
                
                    //Use the below line to change the default
                    //"landing page" of the application.
                    //options.Conventions.AddPageRoute(Path.Combine(Program.DataPath, "www","index.razor") , "");
              
                  
                });*/

            //
            // services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            // {
            //     Program.CORSOptions = o;
            //     builder.AllowAnyOrigin()
            //         .AllowAnyMethod()
            //         .AllowAnyHeader()
            //         .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");
            // }));

            services.AddCors(o => o.AddPolicy( Program.CORSpolicy, builder =>
            {
                builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .WithExposedHeaders("Grpc-Status", "Grpc-Message", "Grpc-Encoding", "Grpc-Accept-Encoding");

                var corsList = new List<string>();
                foreach (var domain in clsStartupConfig.Settings.DomainNames)
                {
                    corsList.Add($"https://{domain}");
                }
                corsList.AddRange(clsStartupConfig.Settings.CorsURL);
                Log.Information("CORS: {@list}", corsList);

                if (corsList.Contains("*") || corsList.Contains("https://*"))
                {
                    Log.Information("CORS: AllowAnyOrigin ACTIVE");
                    builder.AllowAnyOrigin();
                }
                else
                {
                    builder.WithOrigins(corsList.Distinct().ToArray());
                }
            }));

            if (clsStartupConfig.Settings.DomainNames?.Count > 0)
            {
            
                services.AddLettuceEncrypt(c =>
                {
                    c.DomainNames = clsStartupConfig.Settings.DomainNames.ToArray();
                    c.EmailAddress = clsStartupConfig.Settings.DomainEmail;
                    c.AcceptTermsOfService = true;
                    c.RenewDaysInAdvance = TimeSpan.FromDays(5);
                    c.AllowedChallengeTypes = ChallengeType.TlsAlpn01;
#if DEBUG
                    c.UseStagingServer = true;
#endif
                })
                .PersistDataToDirectory(new DirectoryInfo(Path.Combine(Program.DataPathCerts, "certs")), null);
            }
        //    services.AddControllers();
         //   services.AddRouting();

         services.AddSingleton<IProxyConfigProvider>(Program.ProxyConfigProvider);
         services.AddReverseProxy();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseSerilogRequestLogging(options =>
            {
                // Customize the message template
                options.MessageTemplate = "Handled {RequestPath}";
   #if DEBUG
                // Emit debug-level events instead of the defaults
//                options.GetLevel = (httpContext, elapsed, ex) => LogEventLevel.Debug;
   #endif
                // Attach additional properties to the request completion event
                options.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    diagnosticContext.Set("RequestHost", httpContext.Request.Host.Value);
                    diagnosticContext.Set("RequestScheme", httpContext.Request.Scheme);
                 //   diagnosticContext.Set("RequestHeaders", httpContext.Request.Headers);
                 //   diagnosticContext.Set("ResponseHeaders", httpContext.Response.Headers);
                };
            });

            app.UseRouting();
            app.UseGrpcWeb(); 
            app.UseCors();


            if (!String.IsNullOrEmpty(Program.DataPathWWW) && Directory.Exists(Program.DataPathWWW))
            {
                app.UseDefaultFiles(new DefaultFilesOptions {
                    DefaultFileNames = new List<string> { "index.html" }
                });
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(Program.DataPathWWW),
                    //RequestPath = ""
                });
                Log.Information($"Serve static files from {Program.DataPathWWW}");
            }
            else
            {
                Log.Error($"WWW directory does not exist ({Program.DataPathWWW})");
            }

            app.UseAuthentication();
            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
             //   endpoints.MapRazorPages();
             //   endpoints.MapControllers();
                endpoints.MapGrpcService<AuthenticationService>().EnableGrpcWeb().RequireCors(Program.CORSpolicy);
                endpoints.MapGrpcService<ApiService>().EnableGrpcWeb().RequireCors(Program.CORSpolicy);
                
                // endpoints.MapFallbackToPage("/Index.razor");

                /*endpoints.MapGet("/",
                    async context =>
                    {
                        await context.Response.WriteAsync(
                            "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                    });*/


            endpoints.MapReverseProxy(proxyPipeline =>
            {
                /*proxyPipeline.Use((context, next) =>
                {
                    if (context.Request.Headers.Origin.ToString() != null)
                    {
                        string org = context.Request.Headers.Origin.ToString();
                        Log.Information("ORIGIN: {org}",org);
                    }

                    return next();
                });*/
            });
            });

        }
    }
}