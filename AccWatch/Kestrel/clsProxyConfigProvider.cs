using System.Collections;
using Microsoft.Extensions.Primitives;
using Serilog;
using Yarp.ReverseProxy.Configuration;
using Yarp.ReverseProxy.LoadBalancing;
using Yarp.ReverseProxy.Transforms;
using Yarp.ReverseProxy.Transforms.Builder;

namespace AccWatch;

public class clsProxyConfigProvider : IProxyConfigProvider
{
    private volatile CustomMemoryConfig _config;
  //  private Dictionary<string, ClusterConfig> ClusterConfigList = new Dictionary<string, ClusterConfig>();
  //  private Dictionary<string, RouteConfig> RouteConfigList = new Dictionary<string, RouteConfig>();
    public List<uint> ListenPortList = new List<uint>();

    public IProxyConfig GetConfig() => _config;
    
    public clsProxyConfigProvider()
    {
    }

    public clsProxyConfigProvider Build()
    {
        Log.Information("clsProxyConfigProvider build");

        var Routes = new List<RouteConfig>();
        var Clusters = new List<ClusterConfig>();


        Routes.Add( new RouteConfig
        {
            RouteId = "GrafanaWS",
            ClusterId = "GrafanaClusterWS",
      //      CorsPolicy = Program.CORSpolicy,  //https://microsoft.github.io/reverse-proxy/articles/cors.html
            Match = new RouteMatch
            {
                Path = "/grafana/api/live/ws"
            },
            Order = 1
        }.WithTransformUseOriginalHostHeader(true));


       Routes.Add( new RouteConfig
        {
            RouteId = "Grafana",
            ClusterId = "GrafanaCluster",
      //      CorsPolicy = Program.CORSpolicy,  //https://microsoft.github.io/reverse-proxy/articles/cors.html
            Match = new RouteMatch
            {
             //   Hosts = new string[]{ $"*:3030"},
                Path = "/grafana/{**catch-all}"
            },
            Order = 10
        }.WithTransformUseOriginalHostHeader(true));


        //Add listening ports if required
       foreach (var routeConfig in Routes)
       {
           Log.Information($"Proxy: Adding route: {routeConfig.RouteId} {routeConfig.Match.Hosts} {routeConfig.Match.Path} ");
           if (routeConfig.Match?.Hosts != null)
           {
               foreach (var matchHost in routeConfig.Match.Hosts)
               {
                   var portPt = matchHost.LastIndexOf(':');
                   if (portPt > 0)
                   {
                       uint port = 0;
                       if (uint.TryParse(matchHost.Substring(portPt + 1), out port))
                       {
                           if (!ListenPortList.Contains(port)) ListenPortList.Add(port);
                       }
                   }
               }
           }
       }


        var destination1 = new Dictionary<string, DestinationConfig>();
        destination1.Add($"Grafana-3000", new DestinationConfig() { Address = "http://localhost:3000" });
        Clusters.Add( new ClusterConfig()
        {
            ClusterId = "GrafanaCluster",
            LoadBalancingPolicy = LoadBalancingPolicies.RoundRobin,
            Destinations = destination1,
        });

        var destination2 = new Dictionary<string, DestinationConfig>();
        destination2.Add($"Grafana-3000-WS", new DestinationConfig() { Address = "ws://localhost:3000/grafana/api/live/ws" });
        Clusters.Add( new ClusterConfig()
        {
            ClusterId = "GrafanaClusterWS",
            LoadBalancingPolicy = LoadBalancingPolicies.RoundRobin,
            Destinations = destination2,
        });


        foreach (var clusterConfig in Clusters)
        {
            Log.Information($"Proxy: Adding destination: {clusterConfig.ClusterId} {String.Join(",", clusterConfig.Destinations)}");
        }

        _config = new CustomMemoryConfig(Routes, Clusters);
        return this;
    }



    
    /// <summary>
    /// By calling this method from the source we can dynamically adjust the proxy configuration.
    /// Since our provider is registered in DI mechanism it can be injected via constructors anywhere.
    /// </summary>
    public void Update(IReadOnlyList<RouteConfig> routes, IReadOnlyList<ClusterConfig> clusters)
    {
        var oldConfig = _config;
        _config = new CustomMemoryConfig(routes, clusters);
        oldConfig.SignalChange();
    }

    private class CustomMemoryConfig : IProxyConfig
    {
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();

        public CustomMemoryConfig(IReadOnlyList<RouteConfig> routes, IReadOnlyList<ClusterConfig> clusters)
        {
            Routes = routes;
            Clusters = clusters;
            ChangeToken = new CancellationChangeToken(_cts.Token);
        }

        public IReadOnlyList<RouteConfig> Routes { get; }

        public IReadOnlyList<ClusterConfig> Clusters { get; }

        public IChangeToken ChangeToken { get; }

        internal void SignalChange()
        {
            _cts.Cancel();
        }
    }
}