using System.Net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace AccWatch;

public class clsWebHostBuilder :  HostBuilder
{

public clsWebHostBuilder()
{
    var selfCert = SelfSignedCertificate.GetSelfSignedCertificate();


    this.UseSerilog();

    this.ConfigureWebHost(webHost =>
        {
            Log.Information($"Configuring Kestrel");
            webHost.UseKestrel(k =>
            {

                k.ConfigureHttpsDefaults(h =>
                {
                    h.ClientCertificateMode = ClientCertificateMode.AllowCertificate;
                    h.ClientCertificateValidation = (certificate, chain, errors) =>
                    {
                        return true; //certificate.Issuer == serverCert.Issuer;
                    };
                });

                try
                {
                    k.Listen(IPAddress.Any, (int)clsStartupConfig.Settings.WWWPort, options =>
                    {
                        Log.Information("Adding Port: {port}", clsStartupConfig.Settings.WWWPort);
                        options.UseHttps(o =>
                        {
                            if (Program.SSLCert != null)
                            {
                                o.ServerCertificate = Program.SSLCert;
                                Log.Information("Using supplied Cert");
                            }
                            else if (!String.IsNullOrEmpty(clsStartupConfig.Settings.DomainEmail))
                            {
                                o.UseLettuceEncrypt(k.ApplicationServices);
                                Log.Information("Using Lets Encrypt");
                            }
                            else
                            {
                                o.ServerCertificate = selfCert;
                                Log.Information("Using Self Cert");
                            }
                        });
                    });
                }
                catch (Exception e)
                {
                    Log.Error(e,"Setup WWW");
                }


            try
            {
                //Proxy ports
                foreach (var port in Program.ProxyConfigProvider.ListenPortList)
                {
                    k.Listen(IPAddress.Any, (int)port, options =>
                    {
                            Log.Information($"Adding HTTPS Port: {port} (Certified)");
                            options.UseHttps(o =>
                            {
                                o.ClientCertificateMode = ClientCertificateMode.NoCertificate;  //.AllowCertificate;
                                //   o.AllowAnyClientCertificate();

                                if (Program.SSLCert != null) o.ServerCertificateSelector = (context, dnsName) => Program.SSLCert;  //Load dynamically

#if DEBUG
                                else o.ServerCertificate = selfCert;
#else
                                    else o.UseLettuceEncrypt(k.ApplicationServices);
#endif
                            });
                    });
                }
            }
            catch (Exception e)
            {
                Log.Error(e,"Proxy Port Setup");
                throw;
            }
            });

            if (!String.IsNullOrEmpty(Program.DataPathWWW) && Directory.Exists(Program.DataPathWWW)) webHost.UseWebRoot(Program.DataPathWWW);
            webHost.UseStartup<Startup>();
        });
    }

}