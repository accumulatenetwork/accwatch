﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using AccWatch.Monitoring;
using AccWatch.TaskManager;
using AccWatch.TelnetConsole;
using ConsoleServer;
using ConsoleServerDocker;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Serilog;
using Microsoft.Extensions.Hosting;
using Mono.Unix;
using Mono.Unix.Native;
using Serilog.Events;

namespace AccWatch
{
    public class Program
    {
        public static Proto.API.Settings APISettings { get; set; }
        public static string DataPath { get; private set; } 
        public static string DataPathWWW { get; private set; }
        public static string DataPathCerts { get; private set; }
        // public static string[] DomainNames { get; private set; }
        static public clsProxyConfigProvider ProxyConfigProvider = new clsProxyConfigProvider();
        // private static int HttpsPort;
        static public CancellationTokenSource CancelTokenSource = new CancellationTokenSource();
        static public GrpcChannelOptions SelfCertOptions;
        static public readonly string CORSpolicy = "CorsPolicy";
        
        static public DateTime AppStarted = DateTime.UtcNow;
        //static public clsBotClient Bot;
        static public FileSystemWatcher WatchCertsDirectory { get; set; }
        static public bool SaveChanges = false;
        static public bool RestartRequired = false;
        static public bool NewVersion { get; set; }

        static public X509Certificate2? SSLCert = null;
        static public clsNetworkSList NetworkSList;
        static public clsNodeSList NodeSList;
        static public clsNotificationPolicySList NotificationPolicySList;
        static public clsNodeGroupSList NodeGroupSList;
        
        static public Proto.API.NetworkStatus networkStatus;
        static public clsDockerManager DockerManager;
        static public TelnetServer telnetServer;

        static public clsAlarmManager AlarmManager = new clsAlarmManager();
        static public ManualResetEvent ApplicationHold = new ManualResetEvent(false);

        static public DateTime AlarmOffTime;
        static public uint AlarmOffWarningMultiplier;
        public enum EnumAlarmState { Off, On, Silent }
        static public  DateTime? AlarmStateTimeout;
        static public  EnumAlarmState _alarmState = EnumAlarmState.On;
        static public  EnumAlarmState AlarmState 
        {    get
            {
                return _alarmState;
            }
        
            private set
            {
                if (_alarmState != value)
                {
                    _alarmState = value;
                    if (value == EnumAlarmState.Off) //New state off
                    {
                        AlarmOffTime = DateTime.UtcNow;
                        AlarmOffWarningMultiplier=0;
                    }
                    else if (value == EnumAlarmState.On) //New state on
                    {
                        AlarmStateTimeout = null;
                    }
                }
            }
        }
        
        
        public static void Main(string[] args)
        {
            
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .Enrich.FromLogContext()
#if DEBUG
                .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Debug)
#else
            .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
#endif
                //  .WriteTo.File("logfile.log", rollingInterval: RollingInterval.Day)
                .WriteTo.ConsoleTelnetServer()
                .CreateLogger();

            var GitHash = AssemblyGitCommit.GetValue();
            if (!String.IsNullOrEmpty(GitHash))
            {
                Log.Information("Git Hash: {GitHash}",GitHash);
                Log.Information("Git Branch: {Branch}  Tag: {Tag} {date:yyyy-MM-dd HH:mm:ss}",AssemblyGitCommitBranch.GetValue(),AssemblyGitCommitTag.GetValue(),AssemblyGitCommitDateTime.GetValue());
            }

            DataPath = Environment.GetEnvironmentVariable("DATA", EnvironmentVariableTarget.Process) ?? "/data";
            DataPathWWW = Environment.GetEnvironmentVariable("WWW", EnvironmentVariableTarget.Process) ?? "/www";
            DataPathCerts = Environment.GetEnvironmentVariable("CERTS", EnvironmentVariableTarget.Process) ?? "/certs";

            if (!Directory.Exists(DataPath))
            {
#if DEBUG
                DataPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                DataPathCerts = DataPath;

#else
                Log.Fatal("Data path not found: {path}",DataPath);
                return;
#endif
            }

            NetworkSList = new clsNetworkSList();
            NodeGroupSList = new clsNodeGroupSList();
            NotificationPolicySList = new clsNotificationPolicySList();
            NodeSList = new clsNodeSList();

            clsStartupConfig.Load();
            clsSettings.Load();

            WatchCertsDirectory = clsCertificates.WatchCertsDir();

            DockerManager = new clsDockerManager("accwatch","registry.gitlab.com/accumulatenetwork/monitoring/accwatch:latest",CancelTokenSource);

            int TelnetPort;
            if (int.TryParse(Environment.GetEnvironmentVariable("TELNETPORT", EnvironmentVariableTarget.Process) ?? "3322", out TelnetPort))
            {
                telnetServer = new ConsoleServer.TelnetServer(TelnetPort);
                Log.Information("Starting Telnet Server");
                telnetServer.StartListen(new Action<TelnetSession>((session) =>
                {
                    var telnetAppSession = new clsTelnetAppSession(session, Log.Logger);
                    telnetAppSession.Run();
                }));

            }

            SelfCertOptions = new GrpcChannelOptions()
            {
                HttpHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                }
            };

            ProxyConfigProvider.Build();

            const int apiTimeout = 2000;
            const int loopWait = 3000;
            APISettings.BotName ="AccWatch";

            /*
            {
                if (!String.IsNullOrEmpty(Settings.DiscordClientID) && !String.IsNullOrEmpty(Settings.DiscordToken)) Bot.RunAsync();
                if (log.Contains("rror:")) Bot.SendAlert($"```{log}```");
                
                while (RunState == enumRunState.Run)
                {
                    //Query every node.
                    foreach (var node in Program.NodesList.Values.Where(x=>x.Monitor))
                    {
                           node.GetHeightAsync(apiTimeout); 
                    }

                    ApplicationHold.WaitOne(apiTimeout);  //Wait for the timeout
                    
                    foreach (var group in NodeGroupList.Values)
                    {
                        group.Monitor();
                    }
                    
                    foreach(var network in NetworkList.Values)
                    {
                        network.CheckStall();
                    }
                    
                    if (AlarmStateTimeout.HasValue)
                    {
                        if (AlarmStateTimeout.Value < DateTime.UtcNow) AlarmState = EnumAlarmState.On;
                    }
                    else if (AlarmState == EnumAlarmState.Off && (DateTime.UtcNow - AlarmOffTime).TotalMinutes > (AlarmOffWarningMinutes*(AlarmOffWarningMultiplier+1))) 
                    {
                        Bot.Our_BotAlert.SendMessageAsync($"Warning, the Alarm has been off {(DateTime.UtcNow - AlarmOffTime).TotalMinutes:0} minutes.  Forget to reset it?");
                        AlarmOffWarningMultiplier++;
                    }
                    
                   AlarmManager.Process();

                    //Check the status of the Discord connection.  If it disconnects, it doesn't always restart.
                    if (!Bot.LastHeartbeatd.HasValue || (DateTime.UtcNow - Bot.LastHeartbeatd.Value).TotalSeconds > 90)
                    {
                        if ((DateTime.UtcNow - LastBotStart).TotalSeconds > 120)
                        {
                            Console.WriteLine("Discord not connected. Restarting.....");
                            BotErrorCounter++;
                            if (BotErrorCounter == 2) clsEmail.EmailAlertList("Bot: Discord not connected.");
                            Bot._client.Dispose();
                            LastBotStart = DateTime.UtcNow;
                            Bot = new clsBotClient(DiscordToken);
                            Bot.RunAsync();
                        }
                    }
                    else
                    {
                        BotErrorCounter = 0;
                    }

                    clsSSLCertMonitor.HeartbeatCheck();
                  
                   */
               // Log.Information($"Domain: {clsStartupConfig.Settings.DomainNames ?? "Not Set"}");
               // Log.Information($"Port: {HttpsPort}");

               Log.Information($"Start clsWebHostBuilder");
               var hostBuilder = new clsWebHostBuilder();

               var hostTask = hostBuilder.Build().RunAsync(CancelTokenSource.Token);
                
                ApplicationHold.WaitOne(loopWait);

                UnixSignal[] signals = new UnixSignal[] {
                    new UnixSignal(Signum.SIGQUIT),
                    new UnixSignal(Signum.SIGTERM),
                };

                do
                {
                    int id = UnixSignal.WaitAny(signals,5000);
                    if (id >= 0 && id < signals.Length)
                    {
                        if (signals[id].IsSet)
                        {
                            Log.Fatal($"UNIX Signal {signals[id]}");
                            CancelTokenSource.Cancel();
                            break;
                        }
                    }
                    clsTaskManager.Run();
                } while (!CancelTokenSource.IsCancellationRequested);

                WatchCertsDirectory?.Dispose();
                DockerManager?.Dispose();
                telnetServer.Dispose();
                Log.CloseAndFlush();

        }
        
        static public void SendAlert(String message)
        {
           // if (AlarmState == EnumAlarmState.On || AlarmState == EnumAlarmState.Silent)
           //     Bot.Our_BotAlert.SendMessageAsync(message);
        }

        
    }
    
}