using System.Net;
using System.Text.RegularExpressions;
using ConsoleServer;
using ConsoleServerDocker;
using Docker.DotNet.Models;
using Spectre.Console;

namespace AccWatch.TelnetConsole;

public class clsDebug
{
    private AnsiTelnetConsole AnsiConsole;
    private clsTelnetAppSession Root;


    public void Debug(clsTelnetAppSession root)
    {
        Root = root;
        AnsiConsole = root.AnsiConsole;
        bool exit = false;
        
        do
        {
            var list = new List<SelectionFunction<bool>>();

            list.Add(new SelectionFunction<bool>("Reset Data & Restart", () =>
            {
                DeleteConfig();

                Program.CancelTokenSource.Cancel();
                AnsiConsole.MarkupLine("[yellow]Waiting for restart[/]");
                Thread.Sleep(10000);
                return true;
            }));

            list.Add(new SelectionFunction<bool>("Reset Data & Update", () =>
            {

                DeleteConfig();
                var container = Program.DockerManager.UpgradeContainer(AnsiConsole).Result;
                AnsiConsole.MarkupLine("[yellow]Waiting for restart[/]");
                Thread.Sleep(10000);
                Program.CancelTokenSource.Cancel();
                return true;
            }));


            list.Add(new SelectionFunction<bool>("Attach to Log Output", () =>
            {
                var parameters = new ContainerLogsParameters
                {
                    ShowStdout = true,
                    ShowStderr = true,
                    Since = null,
                    Timestamps = true,
                    Follow = true,
                    Tail = "300",
                };
                var container = Program.DockerManager.GetContainer("accwatch").Result;
                if (container != null)
                {
                    byte[] buffer = new byte [1024];
                    using (var logStream = Program.DockerManager.GetClient()?.Containers?.GetContainerLogsAsync(container.ID, parameters).Result)
                    {
                        using (var keypress = new clsKeyPressed(AnsiConsole,new Action<ConsoleKeyInfoExt>(c =>
                        {
                                c.Cancel = true;
                                if (c.Key == ConsoleKey.Escape ||
                                c.Shift ||
                                c.Control)
                            {
                                logStream.Close();
                            }
                        })))
                        {
                            try
                            {
                                if (logStream != null)
                                {
                                    do
                                    {
                                        var count = logStream.Read(buffer, 0, buffer.Length);
                                        if (count <= 0) break;
                                        AnsiConsole.TelnetSession.SendFixCR(buffer, 0, count);
                                    } while (true);
                                }
                            }
                            catch (Exception e)
                            {
                            }
                        }
                    }
                }
                else
                {
                    AnsiConsole.WriteLine("Not Found");
                }
                Root.End();
                return false;
            }));

            list.Add(new SelectionFunction<bool>("Attach BASH Console", () =>
            {
                    var container = Program.DockerManager.GetContainer("accwatch").Result;
                    if (container != null)
                    {
                        using (var conr = new clsDockerAttachToContainer(false,AnsiConsole))
                        {
                            conr.AttachToContainer(container.ID, new[] { "/bin/bash" });
                            conr.AttachConsoleInOut(false).Wait();
                        }
                        Root.End();
                    }
                return false;
            }));

            list.Add(new SelectionFunction<bool>("Update Flutter front end.", () =>
            {
                var container = Program.DockerManager.GetContainer("accwatch").Result;
                if (container != null)
                {
                    using (var conr = new clsDockerAttachToContainer(false,AnsiConsole))
                    {
                        if (conr.AttachToContainer(container.ID, new string[] { "/bin/bash" }))
                        {
                            var task = conr.AttachConsoleOutput(true,3000);
                            conr.AttachConsoleInOut(false);
                            conr.WriteCommand($"cd /").Wait();
                            conr.WriteCommand($"apt update").Wait();
                            conr.WaitForString(TimeSpan.FromSeconds(20),"root@");
                            conr.WriteCommand($"apt install unzip").Wait();
                            conr.WaitForString(TimeSpan.FromSeconds(20),"root@");
                            conr.WriteCommand($"wget https://gitlab.com/api/v4/projects/41009693/jobs/artifacts/main/download?job=build -O web.zip").Wait();
                            conr.WaitForString(TimeSpan.FromSeconds(20),"root@");
                            conr.WriteCommand($"unzip -o -f web.zip").Wait();
                            task.Wait();
                        }
                    }
                }
                else
                {
                    AnsiConsole.WriteLine("Not Found");
                }
                Root.End();
                return false;
            }));

            list.Add(new SelectionFunction<bool>("List Telnet Users", () =>
            {
                foreach (var ipEndPoint in Root.TelnetSession.ConnectionList())
                {
                    AnsiConsole.WriteLine($"{ipEndPoint.Address}:{ipEndPoint.Port}");
                }
                Root.End();
                return false;
            }));

            list.Add(new SelectionFunction<bool>("AccMan logging", () =>
            {
                Root.SetLogLevel();
                return false;
            }));

            list.Add(new SelectionFunction<bool>("<- Back", () => true));

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<bool>>()
                    .Title("CORS")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(list));

            exit = option.MenuAction();

        } while (!exit);
    }

    private void DeleteConfig()
    {
        // if (File.Exists(clsStartupConfig.DataPath))
        // {
        //     Log.Information("Deleting ${clsStartupConfig.DataPath}");
        //     File.Delete(clsStartupConfig.DataPath);
        // }

        if (File.Exists(clsSettings.DataPath))
        {

            ALog.Info("Deleting ${clsSettings.PathSettings}");
            File.Delete(clsSettings.DataPath);
        }

        Program.NetworkSList.ClearDataFile();
        Program.NodeSList.ClearDataFile();
        Program.NodeGroupSList.ClearDataFile();
        Program.NotificationPolicySList.ClearDataFile();

    }


    bool MenuDelete(string prompt)
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("Keep",() => false));
        list.Add(new SelectionFunction<bool>("Delete",() => true));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }

}