using ConsoleServer;

namespace AccWatch.TelnetConsole;

public class clsKeyPressed : IDisposable
{
    private Action<ConsoleKeyInfoExt> _action;
    private AnsiTelnetConsole _console;

    public clsKeyPressed(AnsiTelnetConsole console,  Action<ConsoleKeyInfoExt> action)
    {
        _action = action;
        _console = console;
        _console.Input.KeyPressed += KeyPressed;
    }
    
    public void KeyPressed(object? sender, ConsoleKeyInfoExt e)
    {
        _action.Invoke(e);
    }

    public void Dispose()
    {
        _console.Input.KeyPressed -= KeyPressed;
    }
    
}