using Serilog.Events;
using Spectre.Console;


namespace AccWatch.TelnetConsole;

public partial class clsTelnetAppSession
{
    public void SetLogLevel()
    {
        
        var list = new List<SelectionLoggerItem>();
        list.Add(new SelectionLoggerItem("OFF", SelectEnum.Off,TelnetSession.LogLevel==null));
        list.Add(new SelectionLoggerItem(LogEventLevel.Verbose,TelnetSession.LogLevel==LogEventLevel.Verbose));
        list.Add(new SelectionLoggerItem(LogEventLevel.Debug,TelnetSession.LogLevel==LogEventLevel.Debug));
        list.Add(new SelectionLoggerItem(LogEventLevel.Information,TelnetSession.LogLevel==LogEventLevel.Information));
        list.Add(new SelectionLoggerItem(LogEventLevel.Warning,TelnetSession.LogLevel==LogEventLevel.Warning));
        list.Add(new SelectionLoggerItem(LogEventLevel.Error,TelnetSession.LogLevel==LogEventLevel.Error));
        list.Add(new SelectionLoggerItem(LogEventLevel.Fatal,TelnetSession.LogLevel==LogEventLevel.Fatal));
        list.Add(new SelectionLoggerItem("<- Back", SelectEnum.Exit));
        
        
        var logLevel = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionLoggerItem>()
                .Title("Select [green]Log Level[/]")
                .AddChoices(list));

        switch (logLevel.Selection)
        {
            case SelectEnum.Off:
                AnsiConsole.TelnetSession.LogLevel = null;
                break;
            case SelectEnum.Selection:
                AnsiConsole.TelnetSession.LogLevel = logLevel.LogLevel;
                break;
            case SelectEnum.Exit:
                return;
        }
        
        
        
    }
}