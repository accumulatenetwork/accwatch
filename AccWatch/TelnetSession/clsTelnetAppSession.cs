using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using ConsoleServer;
using Serilog;
using Serilog.Events;
using Spectre.Console;
using Docker.DotNet.Models;
using ConsoleServerDocker;
using ILogger = Serilog.ILogger;

namespace AccWatch.TelnetConsole;

public partial class clsTelnetAppSession
{
    public  TelnetSession TelnetSession { get; init; }
    public AnsiTelnetConsole AnsiConsole => TelnetSession.AnsiConsole;
    private ILogger _logger;

    public clsTelnetAppSession(TelnetSession telnetSession , ILogger logger)
    {
        TelnetSession = telnetSession;
        _logger = logger;
    }

    public void Run()
    {
        bool result;
        do
        {
          //  var volumeResponse = clsAccNetworkMap.NetworkInstalled?.GetVolumeResponse().Result;

            var menulist = new List<SelectionFunction<bool>>();

            menulist.Add(new SelectionFunction<bool>($"Users", () =>
            {
             //   Program.PiIO.ScanTempBus(AnsiConsole).Wait();
                return true;
            }));

            menulist.Add(new SelectionFunction<bool>($"HTTPS Port ({clsStartupConfig.Settings.WWWPort})", () =>
            {
                string portString;
                int port;
                do
                {
                    portString = AnsiConsole.Ask<string>("Enter port number (or 'exit')?");
                    if (String.IsNullOrEmpty(portString) || portString.ToLower().Contains("e")) return true;
                } while (! (int.TryParse(portString,out port) && port > 0 && port <= 65535));

                clsStartupConfig.Settings.WWWPort = (uint)port;
                Program.SaveChanges = true;
                return true;
            }));

            menulist.Add(new SelectionFunction<bool>($"Domain & certificates", () =>
            {
                new clsDomain().Domain(this);
                return true;
            }));

            menulist.Add(new SelectionFunction<bool>(Program.RestartRequired ? "[yellow]AccWatch Restart Required[/]" : Program.NewVersion ? "[yellow]Update AccWatch[/]" : "Restart AccWatch", () =>
            {
                if (Program.SaveChanges)
                {
                    clsStartupConfig.Save();
                    Program.SaveChanges = false;
                }

                var container = Program.DockerManager.UpgradeContainer(AnsiConsole).Result;
                AnsiConsole.MarkupLine("[yellow]Waiting for restart[/]");
                Thread.Sleep(10000);
                return true;

            }));

            menulist.Add(new SelectionFunction<bool>("Debug", () =>
            {
                new clsDebug().Debug(this);
                return true;
            }));


            if (Program.SaveChanges && !Program.RestartRequired)
            {
                menulist.Add(new SelectionFunction<bool>($"[yellow]Save Changes[/]", () =>
                {
                    clsStartupConfig.Save();
                    Program.SaveChanges = false;
                    return true;
                }));
            }


            menulist.Add(new SelectionFunction<bool>("Exit", () => { return false; }));

            ShowScreenHeader();

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<bool>>()
                    .Title("[magenta]Main Menu[/]")
                    .PageSize(20)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(menulist));

            result = option.MenuAction();

        } while (result);
    }

    public void ShowScreenHeader()
    {

        AnsiConsole.Clear(true);

        var rule = new Rule("[white]AccWatch Console[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.WriteLine();
        var grid = new Grid();
        grid.AddColumn();
        grid.AddColumn();

        if (clsStartupConfig.Settings.DomainNames.Count > 0)
        {
            foreach (var domainName in clsStartupConfig.Settings.DomainNames)
            {
                grid.AddRow("Domain: ", $"[green]{domainName}[/]");
            }
        }
        else
        {
            grid.AddRow("Domain: ", $"[green]not set[/]");
        }

        foreach (var CORS in clsStartupConfig.Settings.CorsURL)
        {
            grid.AddRow("CORS: ", $"[green]{CORS}[/]");
        }


   //     grid.AddRow("HW 1: ", $"[green]{Program.PiIO.PWM1Frequency}[/]",$"[green]{Program.PiIO.PWM1DutyCycle:P2}[/]");


        AnsiConsole.Write(grid);
        var rule2 = new Rule( $"[blue]{AssemblyGitCommitTag.GetValue() ?? AssemblyGitCommit.GetValue() ?? File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location).ToString("yyyy-MM-dd HH:mm")}[/]");
        rule2.RuleStyle("blue");
        rule2.RightAligned();
        AnsiConsole.Write(rule2);
    }

    public string MenuActiveMarkup(string input, bool active)
    {
        if (active) return input;
        return $"[grey]{input}[/]";
    }

    public bool AreYouSure(string prompt = "Confirm")
    {
        var rule = new Rule($"[white]{prompt}[/]");
        rule.RuleStyle("red");
        rule.LeftAligned();
        AnsiConsole.Write(rule);
        
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));
        
        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title("Are you sure")
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }

    public bool YesNo(string prompt = "Select")
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("No",() => false));
        list.Add(new SelectionFunction<bool>("Yes",() => true));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }


    public void End()
    {
        var rule = new Rule($"[white]end[/]");
        rule.RuleStyle("blue");
        rule.LeftAligned();
        AnsiConsole.Write(rule);

        AnsiConsole.Prompt(
            new SelectionPrompt<string>()
                .Title("")
                .HighlightStyle(Style.Parse("yellow"))
                .PageSize(3)
                .AddChoices(new[] {
                    "Press Return"
                }));
    }
}