using System.Net;
using System.Text.RegularExpressions;
using ConsoleServer;
using Spectre.Console;

namespace AccWatch.TelnetConsole;

public class clsCORS
{
    private AnsiTelnetConsole AnsiConsole;


    private Regex CORSRegEx = new Regex(@"^((https?:\/\/)?.*?([\w\d-]*\.[\w\d]+))($|\/.*$)");
    
    public void CORS(clsTelnetAppSession root)
    {
        AnsiConsole = root.AnsiConsole;
        bool exit = false;
        
        do
        {
            var list = new List<SelectionFunction<SelectEnum>>();
            foreach (var cors in clsStartupConfig.Settings.CorsURL)
            {
                list.Add(new SelectionFunction<SelectEnum>(cors, () => SelectEnum.Selection));
            }

            list.Add(new SelectionFunction<SelectEnum>("Add New CORS Url",() => SelectEnum.New));
            list.Add(new SelectionFunction<SelectEnum>("<- Back", () => SelectEnum.Exit));

            var option = AnsiConsole.Prompt(
                new SelectionPrompt<SelectionFunction<SelectEnum>>()
                    .Title("CORS")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more options)[/]")
                    .AddChoices(list));

            switch (option.MenuAction())
            {
                case SelectEnum.Selection:
                    var delete = MenuDelete(option.Prompt);
                    if (delete)
                    {
                        clsStartupConfig.Settings.CorsURL.Remove(option.Prompt);
                        Program.SaveChanges = true;
                        Program.RestartRequired = true;
                    }
                    break;
                case SelectEnum.New:
                    CORSNew();
                    break;
                case SelectEnum.Exit:
                    exit = true;
                    break;
            }
        } while (!exit);
    }

    void CORSNew()
    {
        string newCORS = "";
        bool valid;
        do
        {
            var rule = new Rule("[white]CORS[/]");
            rule.RuleStyle("blue");
            rule.LeftAligned();
            AnsiConsole.Write(rule);
            AnsiConsole.MarkupLine("[yellow]Examples:[/]");
            AnsiConsole.MarkupLine("[yellow]  *[/]");
            AnsiConsole.MarkupLine("[yellow]  mydomain.com[/]");
            AnsiConsole.MarkupLine("[yellow]  https://*[/]");
            AnsiConsole.MarkupLine("[yellow]  https://mydomain.com[/]");
            AnsiConsole.MarkupLine("[yellow]  https://*.mydomain.com[/]");
            AnsiConsole.WriteLine();
            newCORS = AnsiConsole.Prompt(
                new TextPrompt<string>(" [green]Enter CORS Domain Name.?[/]")
                    .AllowEmpty() );
            if (String.IsNullOrEmpty(newCORS)) return;

            valid = (newCORS == "*" || newCORS == "https://*"  || CORSRegEx.Match(newCORS).Success);

        } while (!valid);

        if (!newCORS.StartsWith("http")) newCORS = $"https://{newCORS}";
        if (newCORS.EndsWith("/")) newCORS = newCORS.Substring(0, newCORS.Length - 1);

        clsStartupConfig.Settings.CorsURL.Add(newCORS);
        Program.RestartRequired = true;
        Program.SaveChanges = true;
    }

    bool MenuDelete(string prompt)
    {
        var list = new List<SelectionFunction<bool>>();
        list.Add(new SelectionFunction<bool>("Keep",() => false));
        list.Add(new SelectionFunction<bool>("Delete",() => true));

        var option = AnsiConsole.Prompt(
            new SelectionPrompt<SelectionFunction<bool>>()
                .Title(prompt)
                .PageSize(5)
                .AddChoices(list));

        return option.MenuAction();
    }
    
}