using ConsoleServerDocker;
using Serilog;

namespace AccWatch.TaskManager.StartupTasks;

public class CheckNodes : ITaskManagerTask
{
    public bool RunOnStartup { get; } = true;
    public bool AllowDuplicates { get; } = false;
    public ITaskManagerTask.TaskStatus Status { get; }
    public DateTime? StartAfter { get; set; }
    public DateTime? DestroyAfter { get; }
    public TimeSpan? Interval { get; }


    public CheckNodes()
    {
        StartAfter = DateTime.UtcNow;
        Interval = TimeSpan.FromSeconds(5);
    }

    public async Task Run()
    {
        foreach (var node in Program.NodeSList.GetShadowArray().Where(x=>x.ProtoMessage.Monitor))
        {
            await node.Update();
        }



        /*if (!String.IsNullOrEmpty(Program.ThisAccManNode.AccManTag))
        {
            try
            {
                var gitLab = new clsGitLab(ProjectID, ContainerRepoID);
                var images = await gitLab.GetCommits();
                if (images != null && images.Count > 0)
                {
                    if (!Program.ThisAccManNode.AccManTag.Contains(images[0].name))
                    {
                        Log.Information($"Checking Version: New Version found {images[0].name}");
                        Program.NewVersion = true;
                    }
                    else
                    {
                        Log.Information($"Checking Version: {Program.ThisAccManNode.AccManTag} -> {images[0].name}");
                    }
                }
                else
                {
                    Log.Information($"Checking Version: No new version");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"Checking Version");
            }
        }
        else
        {
            Log.Information("Version check failed, as no Git tag found.");
        }*/
    }

}